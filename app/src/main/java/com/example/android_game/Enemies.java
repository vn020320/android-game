package com.example.android_game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

public class Enemies {
    private Bitmap enemy; //image for enemy
    private int x, y, maxX, minX, maxY, minY, speed = 1; //stuff for enemies
    private Rect enemyCollision;

    public Enemies(Context context, int screenX, int screenY) {
        //load sprite for enemies
        enemy = BitmapFactory.decodeResource(context.getResources(), R.drawable.enemy);
        maxX = screenX;
        maxY = screenY;
        minX = 0;
        minY = 0;

        //random coord gen to spawn enemies in random y pos on screen
        Random generator = new Random();
        speed = generator.nextInt(6) + 10;
        x = screenX;
        y = generator.nextInt(maxY) - enemy.getHeight();
        enemyCollision = new Rect(x, y, enemy.getWidth(), enemy.getHeight());
    }


    //getter setter
    public Bitmap getEnemy() {
        return enemy;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public Rect getCollision() {
        return enemyCollision;
    }

    //enemy speed and collision
    public void update(int playerSpeed) {
        x -= playerSpeed;
        x -= speed;

        if (x < minX - enemy.getWidth()) {
            Random generator = new Random();
            speed = generator.nextInt(10) + 10;
            x = maxX;
            y = generator.nextInt(maxY) - enemy.getHeight();

            //hitboxes
            enemyCollision.left = x;
            enemyCollision.top = y;
            enemyCollision.right = x + enemy.getWidth();
            enemyCollision.bottom = y + enemy.getHeight();
        }





    }
}

/*
 note: many graphics for game sourced from here:
https://ansimuz.itch.io/spaceship-shooter-environment
credit to ansimuz
 */
