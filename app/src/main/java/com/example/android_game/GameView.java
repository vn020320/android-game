package com.example.android_game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements Runnable {

    volatile boolean playing; //is the game running or not #
    private Thread gameThread = null;
    private final Player player;
    private final Paint paint;
    private final SurfaceHolder surfaceHolder;
    private int counter = 0;

    private Enemies[] enemies;
    private int enemyCount = 3;




    public GameView(Context context, int screenX, int screenY) {
        super(context);

        player = new Player(context, screenX, screenY);
        surfaceHolder = getHolder();
        paint = new Paint();

        enemies = new Enemies[enemyCount];
        for(int i=0; i<enemyCount; i++) {
            enemies[i] = new Enemies(context, screenX, screenY);
        }
    }


    @Override
    public void run() { //while game is running
        while (playing) {
            update();
            draw();
            control();
        }
    }

    private void update() { //update the location of the character
        player.update();
        for(int i=0; i<enemyCount; i++){
            enemies[i].update(2);

            if (Rect.intersects(enemies[i].getCollision(), player.getPlayerCollision())){
                enemies[i].setX(-1000);
                counter++;
            }
        }
    }

    private void draw() { //render character to field
        if (surfaceHolder.getSurface().isValid()) {
            Canvas canvas = surfaceHolder.lockCanvas();
            canvas.drawColor(Color.BLACK);

            //getters for character png, x pos and y pos in player.java
            canvas.drawBitmap(
                    player.getChar(),
                    player.getPlayerX(),
                    player.getPlayerY(),
                    paint);


            //get enemy png image
            for (int i = 0; i < enemyCount; i++) {
                canvas.drawBitmap(
                        enemies[i].getEnemy(),
                        enemies[i].getX(),
                        enemies[i].getY(),
                        paint
                );
            }

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void control() { //fps control
        try {
            Thread.sleep(17); //frametime value. equates to 60fps (limit)
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) { //when screen is touched, move char up
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP: // equals 1
                player.stopBoost();
                break;
            case MotionEvent.ACTION_DOWN:    //equals 0
                player.startBoost();
                break;
        }

        return true;
    }


        public void pause() { //pause the game when called
            playing = false; //set playing boolean to false
            try { //stop game thread
                gameThread.join();
            } catch (InterruptedException ignored) {
            }
        }

        public void resume() { //resume the game when called
            playing = true; //set playing boolean to true
            gameThread = new Thread(this); //resume game thread
            gameThread.start();
        }


}
