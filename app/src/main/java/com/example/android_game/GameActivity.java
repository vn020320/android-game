package com.example.android_game;

import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;

import androidx.appcompat.app.AppCompatActivity;

public class GameActivity extends AppCompatActivity{

    private GameView gameView;
    MediaPlayer music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        music = MediaPlayer.create(this, R.raw.test);
        music.setLooping(true);
        music.start();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        gameView = new GameView(this, size.x, size.y);
        setContentView(gameView);

    }


    //this will pause the game by calling pause in gameview.java
    //background music will also be stopped
    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
        music.stop();

    }

    //same for resume. not entirely sure what super does but studio complains if its not there
    //background music should continue but doesnt idk
    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
        music.start();

    }
}
