package com.example.android_game;
//that error that tells you when to import a class automatically is very helpful
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Player {

    private final Bitmap player; //player img
    private int playerX, maxY, minY;
    private int playerY;
    private int playerSpd; //player coords and speed
    private boolean jump; //is the player jumping (moving up) or not
    private final int grav = -10;
    private final int min_spd = 1;
    private final int max_spd = 20;
    private Rect playerCollision;

    public Player(Context context, int screenX, int screenY) {
        //starting pos and speed. starts player not jumping
        playerX = 50;
        playerY = 50;
        playerSpd = 1;
        //load ship.png in drawable (character)
        player = BitmapFactory.decodeResource(context.getResources(), R.drawable.ship);

        maxY = 100;
        minY = 0;
        jump = false;
        playerCollision = new Rect(playerX, playerY, player.getWidth(), player.getHeight());


    }

    public void update(){ //called by gameview while running
        if (jump) {
            playerSpd += 2;
        }
        else {
            playerSpd -= 5;
        }

        if (playerSpd > max_spd) {
            playerSpd = max_spd;
        }

        if (playerSpd < min_spd) {
            playerSpd = min_spd;
        }

        playerY -= playerSpd + grav;

        if (playerY < minY) {
            playerY = minY;
        }

        if (playerY < maxY) {
            playerY = maxY;
        }

        playerCollision.left = playerX;
        playerCollision.top = playerY;
        playerCollision.right = playerX + player.getWidth();
        playerCollision.bottom = playerY + player.getHeight();
    }

    public void startBoost() {
        jump = true;
    }

    public void stopBoost() {
        jump = false;
    }

    //auto generated getters. very cool
    public Bitmap getChar() {
        return player;
    }

    public Rect getPlayerCollision() {
        return playerCollision;
    }

    public int getPlayerX() {
        return playerX;
    }

    public int getPlayerY() {
        return playerY;
    }

    public int getPlayerSpd() {
        return playerSpd;
    }
}


/*
 note: many graphics for game sourced from here:
https://ansimuz.itch.io/spaceship-shooter-environment
credit to ansimuz
 */